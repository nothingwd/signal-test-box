/*
@file: SignalSuffixManager.h
@author: ZZH
@date: 2023-02-24
@info: 信号名后缀管理器
*/
#pragma once
#include <cstdint>
#include <cstring>

//内部使用位图管理大量数据的存储
class SignalSuffixManager
{
private:
    uint32_t* pMem;
    size_t numOfCell;

    static const uint32_t bitsPerCell = sizeof(uint32_t) * 8;

protected:

public:
    SignalSuffixManager(const size_t bits = 65535);
    SignalSuffixManager(const SignalSuffixManager& other);
    ~SignalSuffixManager()
    {
        if (nullptr != this->pMem)
            delete[] this->pMem;
    }

    //获取某个数是否被使用
    bool get(const size_t bitPos) const;
    //设置某个数是否被使用
    void set(const size_t bitPos, const bool value);
    //清除所有数
    inline void clear(void) { if (nullptr != this->pMem) std::memset(this->pMem, 0, sizeof(uint32_t) * this->numOfCell); }
    //找到第一个符合val的位置
    size_t findFirst(const bool val = 0) const;
    //获取并占用第一个为0的位置
    inline size_t getFirstZero(void) { size_t pos = this->findFirst(); this->set(pos, true); return pos; }
};
