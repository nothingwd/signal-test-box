<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>LoggerWindow</name>
    <message>
        <location filename="LoggerWindow.cpp" line="33"/>
        <source>clear</source>
        <translation>清除</translation>
    </message>
    <message>
        <location filename="LoggerWindow.cpp" line="36"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="LoggerWindow.cpp" line="47"/>
        <source>Log Window</source>
        <translation>Log窗口</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainWindow.ui" line="14"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="307"/>
        <source>SignalBox</source>
        <translation>信号实验箱</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="29"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="329"/>
        <source>Sample Freq:</source>
        <translation>采样率:</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="64"/>
        <location filename="mainWindow.ui" line="73"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="332"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="335"/>
        <source>KHz</source>
        <translation>KHz</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="68"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="331"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="78"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="333"/>
        <source>MHz</source>
        <translation>MHz</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="86"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="336"/>
        <source>Show Log</source>
        <translation>显示Log</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="106"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="337"/>
        <source>Sample Points:</source>
        <translation>采样点数</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="131"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="339"/>
        <source>For some reason, sample points maximum value is limited to 1024, greater value will be support later</source>
        <translation>出于某些原因, 采样点数最大值为1024, 更大的值以后的版本会支持</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="168"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="341"/>
        <source>Signal List:</source>
        <translation>信号列表:</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="202"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="342"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="221"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="343"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="265"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="344"/>
        <source>Signal Expression:</source>
        <translation>信号表达式</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="319"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="345"/>
        <source>Calculate</source>
        <translation>计算当前信号</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="332"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="346"/>
        <source>Spectrum Mode</source>
        <translation>频谱模式</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="346"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="308"/>
        <source>Add Signal</source>
        <translation>新增信号</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="351"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="309"/>
        <source>Delete Signal</source>
        <translation>删除信号</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="356"/>
        <location filename="mainWindow.ui" line="359"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="310"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="312"/>
        <source>Export Workspace</source>
        <translation>导出工作区</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="364"/>
        <location filename="mainWindow.ui" line="367"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="314"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="316"/>
        <source>Import Workspace</source>
        <translation>导入工作区</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="372"/>
        <location filename="mainWindow.ui" line="375"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="318"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="320"/>
        <source>Delete All</source>
        <translation>删除全部信号</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="380"/>
        <location filename="mainWindow.ui" line="383"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="322"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="324"/>
        <source>Replace non-std Chars</source>
        <translation>替换源码内的非ASCII字符</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="386"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="327"/>
        <source>Alt+Shift+F</source>
        <translation>Alt+Shift+F</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="91"/>
        <source>select file to load</source>
        <translation>选择要导入的文件</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="95"/>
        <location filename="mainWindow.cpp" line="102"/>
        <location filename="mainWindow.cpp" line="110"/>
        <location filename="mainWindow.cpp" line="127"/>
        <location filename="mainWindow.cpp" line="149"/>
        <source>file error</source>
        <translation>文件错误</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="95"/>
        <location filename="mainWindow.cpp" line="149"/>
        <source>not select a file.</source>
        <translation>没有选择文件.</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="102"/>
        <location filename="mainWindow.cpp" line="156"/>
        <source>can`t open: %1</source>
        <translation>无法打开: %1</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="110"/>
        <location filename="mainWindow.cpp" line="127"/>
        <source>file format error</source>
        <translation>文件格式错误</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="145"/>
        <source>select file to save</source>
        <translation>选择保存到的文件</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="156"/>
        <source>file open fail</source>
        <translation>文件打开失败</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="195"/>
        <location filename="mainWindow.cpp" line="298"/>
        <source>Name duplicate</source>
        <translation>命名重复</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="195"/>
        <source>Auto name could only try 32 times, if your workspace have a lot of auto-named signal, this problem will happen, but you can try add signal again to fix it.</source>
        <translation>自动命名只会尝试32次, 如果你的工作区内已经有了许多自动命名的信号, 就有可能会出现此问题, 但是你只需要重新添加信号即可修复此问题</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="298"/>
        <source>This name is same as another signal name which already exists.</source>
        <translation>此名称与现有的另一信号重复</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="303"/>
        <source>Name illegal</source>
        <translation>命名非法</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="303"/>
        <source>This name not match naming rules.</source>
        <translation>此名称不符合命名规则.</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="322"/>
        <source>Calculate point error</source>
        <translation>计算点数错误</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="323"/>
        <source>%1 is a illegal value (valid range[%2 - %3])</source>
        <translation>%1 是非法值 (取值范围[%2 - %3])</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../build/grammar.tab.cpp" line="1506"/>
        <source>Calling undefined signal: %1</source>
        <translation>调用未定义的信号: %1</translation>
    </message>
    <message>
        <location filename="../build/grammar.tab.cpp" line="1531"/>
        <source>Calling the %1 function needs %2 arguments</source>
        <translation>调用 %1 函数需要提供 %2 个参数</translation>
    </message>
    <message>
        <location filename="../build/grammar.tab.cpp" line="1544"/>
        <source>Calling undefined function: %1</source>
        <translation>调用未定义的函数: %1</translation>
    </message>
    <message>
        <location filename="../build/grammar.tab.cpp" line="1838"/>
        <source>Compile failed</source>
        <translation>编译失败</translation>
    </message>
    <message>
        <location filename="../build/grammar.tab.cpp" line="1839"/>
        <source>%1
at %2.%3-%4.%5</source>
        <translation>%1 位于 %2.%3-%4.%5</translation>
    </message>
    <message>
        <location filename="../compiler/ast.cpp" line="27"/>
        <source>Signal recursion overflow, max value is 15, please check if you are using circular reference or a signal reference it self</source>
        <translation>信号引用超过最大嵌套限制, 最大值为15, 请检查代码内是否存在循环引用或一个信号引用了它自身</translation>
    </message>
    <message>
        <location filename="../compiler/compiler.cpp" line="31"/>
        <source>Compile Error</source>
        <translation>编译错误</translation>
    </message>
    <message>
        <location filename="../compiler/InnerLib.cpp" line="28"/>
        <source>cake</source>
        <translation>蛋糕</translation>
    </message>
    <message>
        <location filename="../compiler/InnerLib.cpp" line="28"/>
        <source>cake is a lie !!!</source>
        <translation>蛋糕是个谎言!!!</translation>
    </message>
</context>
</TS>
