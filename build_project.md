# 构建指南

- [1. 环境配置](#1-环境配置)
- [2. Windows平台](#2-windows平台)
  - [2.1. 首选方式](#21-首选方式)
  - [2.2. 手动方式](#22-手动方式)
- [3. Android平台](#3-android平台)

## 1. 环境配置

构建之前, 请先安装`CMake`和`Ninja`, 前者用于产生后者所需的文件, 后者则负责执行文件中的规则, 调用编译器编译和链接程序

然后是`QT`, 本软件使用了`QT 5.15`, 安装时请注意不要安装MSVC编译器版本的, 要安装GCC编译器版本的QT, 同时也要选中`Charts`模块和`GCC 8.1.0`编译器本身

为了能让`CMake`找到`QT`, 还需要将`QT`安装目录下的`Qt5Config.cmake`文件所在的文件夹配置到`CMAKE_PREFIX_PATH`环境变量下, 否则`CMake`在配置工程的时候会在`find_package`语句上报错, 此文件位于`QT安装目录\版本号\mingw81_32或者mingw81_64\lib\cmake\Qt5`

除此之外, 还需要安装`flex`和`bison`, 推荐的方式是安装**虚拟的linux环境**, 比如`Cygwin`, 在安装`Cygwin`时, 选择`flex`和`bison`的软件包即可, 在`Cygwin`安装完成后, 将`Cygwin`的`bin`目录的路径加入到`PATH`环境变量中

如果安装`Cygwin`时没有选择`flex`和`bison`, 可以在安装完成后再次运行安装程序重新选择, 实际上这并不会重新安装, 只会把你对软件包的各种修改应用到已装好的`Cygwin`环境里

在顶层`CMakeLists.txt`文件同级目录下, `CMake`可以支持`CMakePresets.json`和`CMakeUserPresets.json`两个文件用于配置各类参数, 包括配置时的预设、构建时的预设和测试时的预设, 其中`CMakePresets.json`是项目级的预设文件, `CMakeUserPresets.json`是用户级的预设文件, 本项目自带的项目级配置文件内的所有配置均为项目默认配置, 因此并不保证在所有人的开发环境下生效, 如果你需要在自己的开发环境内进行开发, 那么有如下两种方式进行适配, 两种方式适合不同的开发场景, 请自行选择

1. 如果你只想要在本地编译, 或者想要基于本项目进行二次开发, 但并不准备提交代码到本仓库, 那么请直接修改[CMakePresets.json](CMakePresets.json)文件内的工具链路径配置即可, 需要注意的是, 本项目基于GPL-2.0开源协议发布, 也就是说二次开发后仍然需要保持开源, 不得闭源发布
2. 如果你需要向本仓库内提交代码, 那么请新建一个名为`CMakeUserPresets.json`的文件, 然后在此文件内编写你自己的各项配置(可复制项目级配置文件然后修改), 此文件不应当被提交到版本控制系统

由于本软件的支持库是以`git submodule`形式引入的, 所以请在克隆仓库时, 使用`git clone --recursive`命令递归将子模块一起克隆下来, 如果克隆时忘记, 也可以在克隆完成之后在源码路径下执行`git submodule init`和`git submodule update`两条指令来补救

如果你使用`Download ZIP`功能而不是`git clone`, 那么请将[z-fft](https://gitee.com/finalize/z-fft)一同下载, 并解压到`external_libs`路径下, 最终应该形成这样的文件目录结构

![子模块](readme_rc/submodule.png)

## 2. Windows平台

### 2.1. 首选方式

本人使用vscode开发本工程, 所使用到的工作区文件也在源码目录下, 完整下载源码后双击[tiny-signal-box.code-workspace](./tiny-signal-box.code-workspace)文件即可自动打开vscode对代码进行编辑和浏览.工作区文件内包括了vscode的设置项, 构建所用的task以及调试所需要的launch配置, 因此一般来说无需使用者再次配置, 只要你是通过工作区文件打开的工程, 那么你所使用的配置就和我的是一样的

这里推荐将vscode的内置终端替换为`Cygwin`所使用的`bash.exe`, 关于如何配置vscode的内置终端, 请查看[Terminal profiles](https://code.visualstudio.com/docs/editor/integrated-terminal#_terminal-profiles)

如果要在vscode内构建, 请按照工作区文件给出的推荐, 安装推荐的扩展程序(打开工作区时, 如果有一些推荐扩展你没有安装, vscode也会弹窗提示), 此处列出的是扩展程序的ID, 在搜索时可以唯一确定到某个具体的扩展程序而不会出现重名的问题

![推荐的扩展程序的ID](./readme_rc/recommandExt.png)

`CMake Tools`扩展程序默认支持预设文件, 当你在源码路径下配置好了`CMakePresets.json`或`CMakeUserPresets.json`, 那么当你打开工作区之后, 就可以看到左下角可以选择使用哪套预设

![选择预设](readme_rc/cmake_select_preset1.png)

下图为本项目自带的预设, 这些预设只有你修改项目级预设文件后才能保证可用, 否则请选择你自己编写的预设

![选择预设](readme_rc/cmake_select_preset2.png)

选择`Windows`前缀的就是编译Windows平台的应用程序, 选择`Android`前缀就是编译apk包, 带有`Default`后缀的选项的是本项目的默认配置, 除非你修改了项目级的预设文件, 或者你的环境配置与文件中一致, 否则的话这些配置就不能在你的电脑上正常工作

选择配置预设之后, 会自动选择对应的构建预设, 如果你需要编译发行版, 请手动切换配置预设

选择好预设之后, 在vscode内按下<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>B</kbd>快捷键即可开始编译, 若你的生成任务默认快捷键不是这个, 也可以手动执行

编译完成后, 按下<kbd>Crtl</kbd>+<kbd>Shift</kbd>+<kbd>T</kbd>快捷键运行测试任务即可看到程序运行效果

若运行中出现问题, 直接在vscode内调试即可(快捷键<kbd>F5</kbd>), 但是请确保工作区文件内的gdb可执行文件的路径配置正确

如果你想编译出apk文件并在安卓平台上运行本软件, 那么只需要选择`Android`前缀的配置预设即可, 其余步骤与`Windows`相同, 但是前提是你的安卓开发环境配置正确, 详细说明请参阅[3. 安卓平台编译指南](#3-android平台)

### 2.2. 手动方式

首选方式的本质实际上只不过是把命令的调用交给了vscode的扩展程序来做而已, 本质上依然是在控制台执行命令, 因此这个过程我们可以手动来做, 这样就不再依赖于vscode及其扩展程序

cmake的编译过程分为配置(configure)和构建(build)两步, 配置的步骤会生成底层构建系统所需的文件, 例如`Makefile`或`ninja.build`, 而构建的步骤实际上就是调用`make`或`ninja`去编译工程

本软件在Windows下编译exe的的配置命令为`cmake --preset "Windows Debug Default"`, 这里的`Windows Debug Default`是配置预设的名称, 如果你创建了用户级预设文件, 并且准备使用自己的配置, 请改为你自己给定的名字

配置完成之后的构建命令为`cmake --build build --target all`

两条命令执行完成之后, 就可以在build目录下看到可执行文件了

`Android`平台与`Windows`类似, 只需要在配置时传入不同的预设名即可, 构建使用相同的命令, 但是前提是安卓的开发环境要正确配置, 详细说明请参阅[3. Android平台](#3-android平台)

## 3. Android平台

在进行安卓的编译之前, 首先要配置安卓的开发环境, 包括`JDK`、`安卓SDK`、`安卓NDK`以及`QT For Andriod`, 这些环境的配置方式网络上有许多教程, 此处不再赘述

本项目提供的预设文件已经包括了安卓构建所需要设置的所有CMake变量, 你只需要将这些变量中涉及路径的项目配置正确即可

预设文件配置正确之后, 在控制台执行`cmake --preset "Android Debug Default"`即可生成用于编译apk的构建系统

然后执行`cmake --build build --target all`, 等待命令跑完即可在build目录下看到apk文件

除了在控制台手动编译apk以外, 你也可以借助`CMake Tools`扩展程序更为简易的切换到`Android Debug Default`预设项去构建

需要注意的是`QT 5.15`的安卓提供了四种不同的体系结构的库, 源码路径下的预设文件指定的目标平台是armeabi-v7a, 此设置适用于大部分手机, 但是如果你需要在电脑上跑请把`"ANDROID_ABI": "armeabi-v7a"`的设置修改为x86, `"ANDROID_BUILD_ABI_armeabi-v7a": "ON"`修改为OFF, `"ANDROID_BUILD_ABI_x86": "OFF"`修改为ON, 其它体系结构的修改方法类似
