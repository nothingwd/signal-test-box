/*
@file: libManager.h
@author: ZZH
@date: 2022-09-29
@info: 外部库管理器
*/
#pragma once
#include <QMap>
#include <QString>
#include <QLibrary>
#include "libFun.h"

#ifndef ANDROID_ABI
#define ANDROID_ABI "armeabi-v7a"
#endif

class LibManager_t
{
public:
    typedef pLibFunction_t(*LinInitFun_t)(void);

private:
    typedef struct
    {
        LinInitFun_t init;
        void(*exit)(void);
    } LibOps, * pLibOps;

    static QMap<QString, LibOps> libMap;
    static bool isInnerFuncLoaded;

    LibManager_t() {}

    ~LibManager_t()
    {
        for (auto& libOps : this->libMap.values())
        {
            if (nullptr != libOps.exit)
                libOps.exit();
        }
        libMap.clear();
    }

#if defined(DEBUG) || defined(__DEBUG)
    static void importInnerFunc(void);
#endif

    static void loadBasicLibs(void);

protected:

public:
    static inline LibManager_t& getInst()
    {
        static LibManager_t inst;
        return inst;
    }

    static bool LoadLib(const QString& path, const QString& libName);
};
