# 滤波器使用引导

- [1. IIR系列滤波器](#1-iir系列滤波器)
- [2. FIR系列滤波器](#2-fir系列滤波器)
  - [2.1. 调用FIR系数计算函数](#21-调用fir系数计算函数)
  - [2.2. 使用MATLAB进行设计](#22-使用matlab进行设计)

软件自带的滤波库内的函数, 主要分为两种, 即IIR滤波器和FIR滤波器, 其中`lpf`和`hpf`属于IIR滤波器, `average`和`fir`属于FIR滤波器, 两种滤波器的使用方法是不一样的

## 1. IIR系列滤波器

IIR滤波器使用较为简单, 只需要提供采样序列和截止频率即可实现滤波, IIR滤波器的输出为滤波后的序列, 因此可以输入给另一个IIR滤波器再次滤波构成高阶数的IIR滤波器, 例如三阶的IIR滤波可写成如下形式

![iir_lv3](../readme_rc/iir_lv3.png)

IIR系列滤波器目前有`lpf`和`hpf`两个函数可用

## 2. FIR系列滤波器

FIR系列的滤波器需要搭配参数才能使用, 此参数并不是截止频率, 而是一个序列, 是滤波器的冲激响应在时域的采样序列, 此序列与信号序列在时域卷积即可实现滤波(等价于信号频谱和滤波器频谱相乘, 因此可以实现滤波). `fir`函数内部实际上只是做了卷积运算而已, 滤波器的类型(高通或低通), 截止频率等均由参数决定, 因此只需要`fir`这一个函数搭配不同的参数即可实现任意的FIR滤波器

要得到一组FIR滤波器的参数, 可使用多种方法, 不同的方法大同小异, 这里给出两种不同的方法得到FIR滤波器系数

### 2.1. 调用FIR系数计算函数

内置的`filters.dll`函数库除了提供各种滤波器函数以外, 还提供了一个FIR滤波器生成器函数, 名为`gen_fir_coff`, 可以用于产生任意类型的FIR滤波器的系数, 下方是使用此函数产生四种不同类型滤波器系数的示例(此示例以工作区文件的形式提供, 此文件位于[../workspace_demo/gen_all_filter.json](../workspace_demo/gen_all_filter.json))

1. 产生32阶, 截止频率为1000Hz的低通滤波器  
![低通滤波器](readme_rc/gen_lp_demo1.png)  
频域响应为:  
![低通滤波器频域](readme_rc/gen_lp_spectrum1.png)

2. 产生32阶, 截止频率为1000Hz高通滤波器  
![高通滤波器](readme_rc/gen_hp_demo1.png)  
频域响应为:  
![高通滤波器频域](readme_rc/gen_hp_spectrum1.png)

3. 产生32阶, 通带为200-1500Hz的带通滤波器  
![带通滤波器](readme_rc/gen_bp_demo1.png)  
频域响应为:  
![带通滤波器频域](readme_rc/gen_bp_spectrum1.png)

4. 产生32阶, 阻带为200-1500Hz的带阻滤波器  
![带阻滤波器](readme_rc/gen_bs_demo1.png)  
频域响应为:  
![带阻滤波器频域](readme_rc/gen_bs_spectrum1.png)

此函数有四个参数, 分别是滤波器类型, 截止频率1, 截止频率2和阶数

类型的取值范围为`0 - 3`, 分别代表了低通, 高通, 带通和带阻

截止频率1代表滤波器频域的第一个边沿所在的频率, 单位为归一化频率, 取值范围`0 - 0.5`, 四种类型的滤波器均会使用到此参数

截止频率2代表滤波器频域的第二个边沿所在的频率, 单位为归一化频率, 取值范围`0 - 0.5`, 只有带通和带阻类型的滤波器会使用此参数, 且此参数的值必须大于截止频率1

阶数代表滤波器的阶数, 阶数越高, 滤波器的滚降越陡峭, 但滤波时的计算量也更大

### 2.2. 使用MATLAB进行设计

内置的滤波器生成器仅能使用汉明窗, 而MATLAB可以提供更多的窗函数和设计方法, 从而得到更加定制化的滤波器

设计目标为一个32阶的FIR低通滤波器, 采样频率5KHZ, 截止频率2KHZ

首先, 打开MATLAB, 切换到APP选项卡, 找到`Filter Designer`应用程序, 并打开它

![filter_designer](../readme_rc/matlab_app.png)

然后在APP内按照我们的设计目标调整参数, 调整完成之后点击`Design Filter`按钮即可得到滤波器的频率响应

![filter_param](../readme_rc/matlab_filter_param.png)

然后点击左上角的`file`, 在菜单内选择`Export`

![export_step1](../readme_rc/matlab_generate_step1.png)

在弹出的窗口内, 按下图设置导出目标, 然后点击`Export`按钮, 然后选择导出到的文件

![export_step2](../readme_rc/matlab_generate_step2.png)

关闭MATLAB, 打开刚才导出的文件可以看到如下内容, 下面的数字就是我们需要的参数

![export_step3](../readme_rc/matlab_generate_step3.png)

本软件支持的参数文件格式为每行一个参数, 因此需要调整下格式, 首先删除数字上面的无用内容, 然后删除所有的空格, 确保第一行不是空行并且每行一个数字即可

![export_step4](../readme_rc/matlab_generate_step4.png)

保存此文件, 并按照`类型_截止频率_采样率`的格式重命名文件, 不要带后缀名, 本案例应命名为`lp_2k_5k`, 参数文件制作完成, 可用`read_file`函数读入到软件内提供给`fir`函数使用

滤波器的类型包括`低通(lp)`、`高通(hp)`、`带通(bp)`和`带阻(bs)`
