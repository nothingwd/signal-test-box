/*
@file: calculatorConf.h
@author: ZZH
@date: 2023-02-22
@info: 语法树计算器配置项
*/
#pragma once

#ifndef BASIC_TYPE
#define BASIC_TYPE float
#endif // !BASIC_TYPE

class CalculatorConf
{
private:
    static unsigned int sizeOfBasicType;
    static unsigned int sizeOfBiggestType;
    static unsigned int sizeFactor;

protected:

public:
    CalculatorConf() {}
    ~CalculatorConf() {}

    //将宏定义换为C++语法, 传递类型信息
    using BasicType = BASIC_TYPE;

    //获取基本数据类型的大小
    static inline unsigned int getBasicTypeSize(void) { return sizeOfBasicType; }
    //获取最大类型的大小
    static inline unsigned int getBiggestTypeSize(void) { return sizeOfBiggestType; }
    //获取最大类型大小与基本类型大小之间的倍数关系
    static inline unsigned int getSizeFactor(void) { return sizeFactor; }
};
