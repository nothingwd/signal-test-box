/*
@file: bufferAllocator.cpp
@author: ZZH
@date: 2023-02-23
@info:
*/
#include <cstdlib>
#include <QMessageBox>
#include "log.h"
#include "bufferAllocator.h"

CalculatorConf BufferAllocator::conf;
const char* BufferAllocator::messageFormat = "not enough memory to allocate %1 byte buffer";

void* BufferAllocator::getRawBuffer(const size_t size)
{
    void* p = malloc(size);

    if (nullptr == p)
    {
        QString msg(messageFormat);
        msg = msg.arg(size);

        COMP_ERROR("%s", msg.toStdString().c_str());
        // QMessageBox::critical(nullptr, "memory", msg);
        throw std::bad_alloc();
    }

    memset(p, 0, size);

    return p;
}
